<?php

/***********************************************************
*                       LOCAL TASK                         *
************************************************************/

function composite_general_select_page($type, $node) {
  $type_def = composite_get_types($type);
  drupal_set_title(t('@label for @title', array('@label' => $type_def['label']['plural'], '@title' => $node->title)));
  return drupal_get_form('composite_general_select_form', $type_def, $node);
}

function composite_general_select_form(&$form_state, $type, $node) {
  // If there is no potentials callback defined, then there's no point continuing. 
  $potentials_callback = $type['potentials callback'];
  composite_include_file($type);
  if (!$potentials_callback || !function_exists($potentials_callback)) {
    return;
  }

  // Save for later use
  $form['#node'] = $node;
  $form['composite_type'] = array('#type' => 'value', '#value' => $type['type']);

  // Retrieve select list of potentials
  $options = $potentials_callback($node);
  
  // Fill in our selected values
  $selected = array();
  foreach ($node->composite_references['#'. $type['type'] .'_references'] as $id => $unused) {
    // Make sure this block still exists
    if (array_key_exists($id, $options)) {
      $selected[$id] = $id;
    }
  }
  
  $form['composite_items'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Items to display in @title', array('@title' => $node->title)),
    '#options' => $options,
    '#multiple' => TRUE,
    '#description' => t('Please select the items you would like to display as part of this node.'),
    '#default_value' => $selected,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;  
}

function composite_general_select_form_submit($form, &$form_state) {
  $node = $form['#node'];
  $type = composite_get_types($form_state['values']['composite_type']);
  
  // Construct a list of items to delete, and a list of new items to insert
  $delete_items = array();
  $insert_items = array();
  foreach ($form_state['values']['composite_items'] as $id => $flag) {
    if ($flag && !$node->composite_references['#'. $type['type'] .'_references'][$id]) {
      // This is an added item, add to in the insert list
      $insert_items[$id] = $id;
    }
    else if (!$flag && $node->composite_references['#'. $type['type'] .'_references'][$id]) {
      // This is a to-be-deleted item, add to the delete list
      $delete_items[$id] = $id;
    }
  }

  // Delete old items  
  foreach ($delete_items as $id => $flag) {
    db_query("DELETE FROM {node_composite_references} WHERE vid = %d AND id = '%s'", $node->vid, $id);
  }

  // Add new entries
  $args = array();
  $query_parts = array();
  foreach ($insert_items as $id => $flag) {
    $query_parts[] = " (%d, %d, '%s', %d, '%s', '%s', '%s')";
    $args[] = $node->nid;
    $args[] = $node->vid;
    $args[] = $type['type'];
    $args[] = 0;
    $args[] = $id;
    $args[] = '';
    $args[] = COMPOSITE_ZONE_NONE;      
  }

  if (count($query_parts)) {
    $query = "INSERT INTO {node_composite_references} (nid, vid, type, weight, id, data, zone) VALUES" . implode(', ', $query_parts);
    db_query($query, $args);
  }
  drupal_set_message(t('The selected items have been saved.'));
}

/***********************************************************
*                          ZONES                           *
************************************************************/

function composite_zones_page($node) {
  drupal_set_title(t('Zones for @title', array('@title' => $node->title)));
  if ($node->composite_layout) {
    $output .= drupal_get_form('composite_zones_form', $node);
    // $output .= theme('composite_zones_preview', node_view($node, FALSE, FALSE, FALSE));
    return $output;
  }
  else {
    return t('You need to choose a layout first.');
  }
}

function composite_zones_form(&$form_state, $node) {
  if ($form_state['operation'] == 'delete') {
    return composite_zones_multiple_delete_confirm($form_state, $node);
  }

  drupal_add_css(drupal_get_path('module', 'block') .'/block.css', 'module', 'all');

  $form['#tree'] = TRUE;

  if (isset($form_state['node'])) {
    // Use the node override instead of the default
    $node = $form_state['node'];
  }
  // Insert preview if it exists
  if (isset($form_state['node_preview'])) {
    $form['#prefix'] = $form_state['node_preview'];
  }

  // Save for later
  $form['#node'] = $node;
  
  $layout = composite_get_layout($node->composite_layout);
  $layout_zones = $layout['zones'] + array(COMPOSITE_ZONE_NONE => t('<none>'));
  // Need to pass this through to template function
  $form['#zones'] = $layout_zones;

  $references = _composite_references_preprocess($node->composite_references, $layout_zones);
  _composite_compare('reset-zones', $layout_zones);
  usort($references, '_composite_compare');

  foreach ($references as $id => $reference) {
    $form[$id]['id'] = array('#type' => 'value', '#value' => $reference['id']);
    $form[$id]['type'] = array('#type' => 'value', '#value' => $reference['type']);
    $form[$id]['nid'] = array('#type' => 'value', '#value' => $node->nid);
    $form[$id]['vid'] = array('#type' => 'value', '#value' => $node->vid);
    $form[$id]['info'] = array('#value' => $reference['info']);
    $form[$id]['weight'] = array(
      '#type' => 'weight',
      '#default_value' => $reference['weight'],    
    );
    $form[$id]['zone'] = array(
      '#type' => 'select',
      // If the reference is in a zone that no longer exists, place it in the disabled list. 
      '#default_value' => array_key_exists($reference['zone'], $layout_zones) ? $reference['zone'] : '-1',
      '#options' => $layout_zones,
    );
    $extra_fields = composite_invoke_referenceapi($reference, 'settings', $node);
    if ($extra_fields) {
      $form[$id]['data'] = $extra_fields;
      // Kludgey
      $form[$id]['data']['#prefix'] = '<div class="container-inline">';
      $form[$id]['data']['#suffix'] = '</div>';
    }
    $form[$id]['checkbox'] = array(
      '#type' => 'checkbox',
      '#default_value' => 0,
    );
  }

  $form['update_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );
  $form['update_options']['operation'] = array(
    '#type' => 'select',
    '#options' => array('delete' => t('Delete checked items')),
  );
  $form['update_options']['update'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#submit' => array('composite_zones_form_operations'),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save zones'),
    '#submit' => array('composite_zones_form_submit'),
  );
  $form['save_and_view'] = array(
    '#type' => 'submit',
    '#value' => t('Save zones and view'),
    '#submit' => array('composite_zones_form_submit'),
  );
  $form['buttons']['preview'] = array(
    '#type' => 'submit',
    '#value' => t('Preview'),
    '#submit' => array('composite_zones_form_preview'),
  );
  return $form;
}

function composite_zones_form_submit($form, &$form_state) {
  foreach ($form_state['values'] as $reference) {
    if (is_array($reference) && isset($reference['id'])) {
      $data = $reference['data'] ? serialize($reference['data']) : '';
      db_query("UPDATE {node_composite_references} SET weight = %d, data = '%s', zone = '%s' WHERE vid = %d AND id = '%s'", $reference['weight'], $data, $reference['zone'], $reference['vid'], $reference['id']);
    }
  }
  drupal_set_message(t('The selected zones have been saved.'));
  if ($form_state['values']['op'] == t('Save zones and view')) {
    $node = $form['#node'];
    $form_state['redirect'] = 'node/' . $node->nid;
  }
}

function composite_zones_form_preview($form, &$form_state) {
  $node = $form['#node'];

  // Rebuild composite references with the submitted values
  $original_references = $node->composite_references;
  $node->composite_references = array();
  
  foreach ($form_state['values'] as $reference) {
    if (is_array($reference) && isset($reference['id'])) {
      // Fill in values which were not transferred with the form (and
      //   implicitly, remained unchanged). 
      $reference['type'] = $original_references[$reference['id']]['type'];
      composite_invoke_referenceapi($reference, 'load');
      
      $node->composite_references[$reference['id']] = $reference;
    }
  }

  // Save values for the next form iteration
  $form_state['node'] = $node;
  $form_state['node_preview'] = theme('composite_zones_preview', drupal_clone($node));

  drupal_set_title(t('Preview'));
  drupal_set_message('This is a preview. Changes will not be saved until the Save zones button is clicked.');

  // Setting this flag seems to propagate ['node_preview'] back to the form builder
  $form_state['rebuild'] = TRUE;
}

function composite_zones_form_operations($form, &$form_state) {
  if ($form_state['values']['update_options']['operation']) {
    $form_state['operation'] = $form_state['values']['update_options']['operation'];
    // Cycle this form
    $form_state['rebuild'] = TRUE;
  }  
}

function composite_zones_multiple_delete_confirm(&$form_state, $node) {
  $form['#node'] = $node;
  $form['references'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  $checklist = array();
  foreach ($form_state['values'] as $reference) {
    if (is_array($reference) && isset($reference['id']) && $reference['checkbox']) {
      composite_invoke_referenceapi($reference, 'load');
      composite_invoke_referenceapi($reference, 'info');

      $form['references'][$reference['id']] = array(
        '#type' => 'hidden',
        '#value' => $reference['id'],
        '#prefix' => '<li>',
        '#suffix' => $reference['info'] ."</li>\n",
      );
    }
  }
  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');  
  $form['#submit'][] = 'composite_zones_multiple_delete_confirm_submit';
  return confirm_form($form,
                      t('Are you sure you want to delete these items?'),
                      'node/' . $node->nid . '/composite_zones', '',
                      t('Delete all'), t('Cancel'));
}

function composite_zones_multiple_delete_confirm_submit($form, &$form_state) {
  $node = $form['#node'];
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['references'] as $id => $reference) {
      db_query("DELETE FROM {node_composite_references} WHERE vid = %d AND id = '%s'", $node->vid, $id);
    }
    drupal_set_message(t('The items have been deleted.'));
  }
  $form_state['redirect'] = 'node/' . $node->nid . '/composite_zones';
  return;
}


// This function borrows some code from template_preprocess_block_admin_display_form
function template_preprocess_composite_zones_form(&$variables) {
  $layout_zones = $variables['form']['#zones'];
  $layout_zones[COMPOSITE_ZONE_NONE] = t('Disabled');
  $variables['layout_zones'] = $layout_zones;
  
  foreach ($layout_zones as $key => $value) {
    // Highlight regions on page to provide visual reference.
    // drupal_set_content($key, '<div class="block-region">'. $value .'</div>');
    // Initialize an empty array for the region.
    $variables['zone_listing'][$key] = array();
  }
  $variables['zone_listing'][COMPOSITE_ZONE_NONE] = array();

  foreach (element_children($variables['form']) as $i) {
    $reference = &$variables['form'][$i];

    // Only take form elements that are references.
    if (isset($reference['id'])) {
      // Fetch zone for current block.
      $zone = $reference['zone']['#default_value'];

      // Set special classes needed for table drag and drop.
      $variables['form'][$i]['zone']['#attributes']['class'] = 'block-region-select block-region-'. $zone;
      $variables['form'][$i]['weight']['#attributes']['class'] = 'block-weight block-weight-'. $zone;

      $variables['zone_listing'][$zone][$i]->row_class = isset($reference['#attributes']['class']) ? $reference['#attributes']['class'] : '';
      $variables['zone_listing'][$zone][$i]->reference_modified = isset($reference['#attributes']['class']) && strpos($reference['#attributes']['class'], 'block-modified') !== FALSE ? TRUE : FALSE;
      $variables['zone_listing'][$zone][$i]->reference_title =  drupal_render($reference['info']);
      $variables['zone_listing'][$zone][$i]->zone_select = drupal_render($reference['zone']); //. drupal_render($block['theme']);
      $variables['zone_listing'][$zone][$i]->weight_select = drupal_render($reference['weight']);
      $variables['zone_listing'][$zone][$i]->data_widget = drupal_render($reference['data']);
      $variables['zone_listing'][$zone][$i]->checkbox = drupal_render($reference['checkbox']);
      $variables['zone_listing'][$zone][$i]->printed = FALSE;
    }
  }

  $variables['form_submit'] = drupal_render($variables['form']);
}
